import styled, { css } from 'styled-components'
import { brownBrightColor, bgSecondaryColor } from '../../globalStyles'
import { ContainerProps } from './container'

export const DivStyled = styled.div<Required<Pick<ContainerProps, 'size'>>>(
    ({ size }) => css`
        border: 9px solid ${brownBrightColor};
        border-radius: 26px;
        background: ${bgSecondaryColor};
        width: ${size === 'sm' ? '30vw' : '60vw'};
        max-width: ${size === 'sm' ? '385px' : '870px'};
        min-height: 670px;
        margin: 100px auto;

        @media screen and (min-width: 1024px) and (max-width: 1339px) {
            width: ${size === 'sm' ? '40vw' : '50vw'};
        }

        @media screen and (max-width: 1023px) {
            width: 90%;
        }
    `
)
