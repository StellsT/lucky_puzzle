import React, { FC } from 'react'
import { DivStyled } from './container.styled'
import type { TSize } from '../../@types/common-types'

export interface ContainerProps {
    size?: TSize
}

const Container: FC<ContainerProps> = ({ children, size = 'md' }) => {
    return (
        <DivStyled size={size}>
            {children}
        </DivStyled>
    )
}

export default Container
