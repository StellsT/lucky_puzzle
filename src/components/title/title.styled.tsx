import { titleHeight } from '../../globalStyles'
import styled, { css } from 'styled-components'

interface TitleStyledProps {
    bgImage: string
}

export const TitleStyled = styled.h2<TitleStyledProps>(
    ({ bgImage }) => css`
        width: 100%;
        height: ${titleHeight};
        background: center/100% ${titleHeight} url(${bgImage}) no-repeat;
        box-sizing: border-box;
        margin-top: calc(${titleHeight} / -2);
        padding-left: 20%;
        padding-right: 20%;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
    `
)
