import React, { FC } from 'react'
import { TitleStyled } from './title.styled'
import Icons from '../../assets/icons'

interface TitleProps {
    background?: string
}

const Title:FC<TitleProps> = ({ children, background = 'title' }) => {
    return (
        <TitleStyled bgImage={Icons[background]}>
            {children}
        </TitleStyled>
    )
}

export default Title
