import React from 'react'
import Icons from '../../assets/icons'
import treeBackground from '../../assets/icons/tree-background.svg'
import extraTreeBackground from '../../assets/icons/extra-tree-background.svg'
import {
    StyledButton,
    StyledButtonBackground,
    StyledInnerButton,
    StyledButtonBorder,
    StyledSvg, 
    StyledExtraArea,
    StyledText
} from './button.styled'
import { bgSecondaryColor } from '../../globalStyles'

export interface ButtonProps {
    icon: string;
    size: number;
    onClick?: (event?: React.MouseEvent) => void;
    background?: 'tree';
    withExtraArea?: boolean;
    resourceValue?: number;
}

export const backgroundMapping = {
    tree: {
        color: bgSecondaryColor,
        background: treeBackground,
        extraBackground: extraTreeBackground
    },
}

const Button = React.memo(({ icon, size, onClick, background, withExtraArea, resourceValue }: ButtonProps) => {
    return (
        <>
            <StyledButton size={size} onClick={onClick} aria-label={icon} withExtraArea={withExtraArea}>
                <StyledButtonBackground size={size} background={background} />
                <StyledInnerButton size={size} icon={Icons[icon]} background={background} className="innerButton"/>
                <StyledButtonBorder size={size} className="border"/>
                {withExtraArea &&
                <StyledExtraArea size={size} background={background} className="extraArea">
                    <StyledText>{resourceValue}</StyledText>
                </StyledExtraArea>}
            </StyledButton>
            <StyledSvg>
                <clipPath id="button-shape" clipPathUnits="objectBoundingBox">
                    <path
                        d="M1,0.5 C1,0.51,0.998,0.519,0.994,0.528 C0.989,0.54,0.982,0.551,0.975,0.561
                        C0.962,0.58,0.948,0.599,0.943,0.619 C0.936,0.645,0.942,0.678,0.942,0.707
                        C0.942,0.723,0.94,0.738,0.933,0.75 C0.931,0.754,0.928,0.757,0.926,0.76
                        C0.9,0.788,0.851,0.798,0.824,0.824 C0.795,0.854,0.787,0.912,0.75,0.933
                        C0.714,0.954,0.66,0.932,0.619,0.943 C0.579,0.953,0.543,1,0.5,1
                        C0.457,1,0.421,0.953,0.381,0.943 C0.34,0.932,0.286,0.954,0.25,0.933
                        C0.214,0.912,0.205,0.854,0.176,0.824 C0.146,0.795,0.088,0.786,0.067,0.75
                        C0.066,0.749,0.065,0.747,0.065,0.746 C0.06,0.734,0.057,0.721,0.058,0.708
                        C0.057,0.679,0.064,0.646,0.057,0.619 C0.047,0.579,0,0.543,0,0.5
                        C0,0.457,0.047,0.421,0.057,0.381 C0.068,0.34,0.046,0.286,0.067,0.25
                        C0.088,0.214,0.146,0.205,0.176,0.176 C0.205,0.146,0.214,0.088,0.25,0.067
                        C0.286,0.046,0.34,0.068,0.381,0.057 C0.421,0.047,0.457,0,0.5,0
                        C0.543,0,0.579,0.047,0.619,0.057 C0.66,0.068,0.714,0.046,0.75,0.067
                        C0.786,0.088,0.794,0.143,0.822,0.173 C0.823,0.174,0.823,0.175,0.824,0.176
                        C0.845,0.197,0.881,0.207,0.908,0.225 C0.918,0.231,0.927,0.24,0.933,0.25
                        C0.954,0.286,0.932,0.34,0.943,0.381 C0.953,0.421,1,0.457,1,0.5">
                    </path>
                </clipPath>
            </StyledSvg>
        </>
    )
})

export default Button
