import styled, { css } from 'styled-components'
import leaves from '../../assets/icons/leaves.svg'
import plus from '../../assets/icons/plus.svg'
import extraArea from '../../assets/icons/extra-button-area.svg'
import { backgroundMapping, ButtonProps } from './button'
import { beigeColor, bgSecondaryLightColor, brownBrightColor } from "../../globalStyles"

const OFFSET = 3
const EXTRA_AREA_WIDTH_RATIO = 1.5
const EXTRA_AREA_HEIGHT_RATIO = 0.5
const BUTTON_EXTRA_WIDTH_RATIO = 2
const BUTTON_EXTRA_HEIGHT_RATIO = 0.8
const PLUS_SIZE_RATIO = 0.35
const PLUS_OFFSET_RATIO = 1.2

export const StyledButton = styled.button<Pick<ButtonProps, 'size' | 'withExtraArea'>>`
  box-sizing: content-box;
  position: relative;
  ${({ size, withExtraArea }) => css`
    width: ${size}px;
    height: ${size}px;
    background: url(${leaves}) transparent center/${size}px no-repeat;
    margin-right: ${withExtraArea && `${size*EXTRA_AREA_WIDTH_RATIO}px`};
  `}
  padding: 5px;
  border: none;
  outline: none;

  display: flex;
  
  &:hover {
    cursor: pointer;
  }
  
  &:active > .innerButton,
  &:active > .border {
    transform: translate(-50%, calc(-50% + 2px));
  }

  &:after {
    content: '';
    position: absolute;
    ${({ size, withExtraArea }) => css`
      width: ${size*PLUS_SIZE_RATIO}px;
      height: ${size*PLUS_SIZE_RATIO}px;
      right: -${size*PLUS_OFFSET_RATIO}px;
      top: 30%;
      display: ${withExtraArea ? 'block' : 'none'};
    `}
    background: center/contain url(${plus}) transparent no-repeat;
  }
`

export const StyledButtonBackground = styled.div<Pick<ButtonProps, 'size' | 'background'>>`
  ${({ size }) => css`
    width: ${size}px;
    height: ${size}px;
  `}
  background-color: ${({ background }) => backgroundMapping[background]?.color || bgSecondaryLightColor};
  clip-path: url(#button-shape);
  margin: 0;
  z-index: 1;
`

export const StyledInnerButton = styled.div<Pick<ButtonProps, 'size' | 'icon' | 'background'>>`
  clip-path: url(#button-shape);
  position: absolute;
  ${({ size, icon, background }) => css`
    width: ${size - OFFSET}px;
    height: ${size - OFFSET}px;
    background-image: url(${icon}), url(${backgroundMapping[background]?.background});
    background-repeat: no-repeat, no-repeat;
    background-size: ${size / 1.7}px, ${size}px;
    background-color: ${backgroundMapping[background]?.color || bgSecondaryLightColor};
    background-position: center, center;
  `}
  top: calc(50% - 4px);
  left: 50%;
  transform: translate(-50%, -50%);
  transition: all 0.1s ease;
  z-index: 3;
`

export const StyledButtonBorder = styled.div<Pick<ButtonProps, 'size'>>`
  clip-path: url(#button-shape);
  position: absolute;
  background: ${brownBrightColor};
  top: calc(50% - 3px);
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  ${({ size }) => css`
    width: ${size - OFFSET}px;
    height: ${size - OFFSET}px;
  `}
`

export const StyledSvg = styled.svg`
  position: absolute;
  width: 0;
  height: 0;
`

export const StyledExtraArea = styled.div<Pick<ButtonProps, 'size' | 'background'>>`
  ${({ size, background }) => css`
    width: ${size*BUTTON_EXTRA_WIDTH_RATIO}px;
    height: ${size*BUTTON_EXTRA_HEIGHT_RATIO}px;
    mask: url(${extraArea}) no-repeat center/${size*EXTRA_AREA_WIDTH_RATIO}px, ${size*EXTRA_AREA_HEIGHT_RATIO}px;
    background: url(${backgroundMapping[background]?.extraBackground});
    background-color: ${backgroundMapping[background]?.color || bgSecondaryLightColor};
    background-size: ${size*EXTRA_AREA_WIDTH_RATIO}px, ${size*EXTRA_AREA_HEIGHT_RATIO}px;
  `}
  position: absolute;
  top: 12%;
  left: 40%;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const StyledText = styled.p`
  font-size: 36px;
  color: ${beigeColor};
  margin: 0;
  padding: 0;
`