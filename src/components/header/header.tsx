import React, { useCallback } from 'react'
import { useNavigate } from 'react-router-dom'
import { URLs } from 'src/__data__/urls'
import Button from '../button'
import { HeaderStyled } from './header.styled'

const Header = () => {
    const navigate = useNavigate()
    
    const handleResourceButtonClick = useCallback(() => {
        navigate(URLs.shop)
    }, [navigate])

    return (
        <HeaderStyled>
            <Button size={100} icon="gear" background="tree" />
            <Button
                size={85}
                icon="dollar"
                background="tree"
                withExtraArea
                resourceValue={1430}
                onClick={handleResourceButtonClick}
            />
            <Button
                size={85}
                icon="energy"
                background="tree"
                withExtraArea
                resourceValue={5}
                onClick={handleResourceButtonClick}
            />
        </HeaderStyled>
    )
}

export default Header
