import { configureStore, combineReducers, getDefaultMiddleware } from "@reduxjs/toolkit"
import { api } from "./api"

const appReducers = {
    // Сюда редьюсеры api и слои
    [api.reducerPath]: api.reducer,
}


export const store = configureStore({
    reducer: combineReducers(appReducers),
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(api.middleware)
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch