import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

interface Response {
    success: boolean
    output?: {
        fields: Array<{
            id: string,
            icon?: string
        }>
        images: Record<string, string>
        titles: Record<string, string>
    },
    error?: string
}

export const api = createApi({
    reducerPath: 'luckyPuzzleApi',
    baseQuery: fetchBaseQuery({ baseUrl: 'api' }),
    endpoints: (builder) => ({
        getDashboard: builder.query<Response, void>({
            query: () => ({ url: '/dashboard' })
        })
    }),
})

export const { useGetDashboardQuery } = api