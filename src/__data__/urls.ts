const baseUrl = '/lucky-puzzle'
const auth = `${baseUrl}/auth`
const shop = 'shop'

export const URLs = {
    baseUrl,
    auth,
    shop
}