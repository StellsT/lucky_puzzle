import React from 'react'
import { getAllFeatures } from '@ijl/cli'
import { Outlet, Route, Routes } from 'react-router-dom'

import { URLs } from './__data__/urls'
import { Header, Loader } from './components'
import { Dashboard, Shop } from './features'

const features = getAllFeatures()['lucky-puzzle']

const Layout: React.FC = () => (
    <>
        <Header />
        <Outlet />
    </>
)

const RouterApp: React.FC = () => (
    <React.Suspense fallback={<Loader />}>
        <Routes>
            <Route path={URLs.baseUrl} element={<Layout />}>
                {features.dashboard && <Route index element={<Dashboard />} />}
                {features.shop && <Route path={URLs.shop} element={<Shop />} />}
            </Route>

            {features.auth && (
                <Route path={URLs.auth} element={<div>auth</div>} />
            )}
            <Route path="*" element={<div>Not Found</div>} />
        </Routes>
    </React.Suspense>
)

export default RouterApp
