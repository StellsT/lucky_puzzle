import React from 'react'
import GlobalStyle from './globalStyles'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import RouterApp from './router'
import { store } from './__data__/store'


const App = () => (
    <Provider store={store}>
        <BrowserRouter>
            <GlobalStyle />
            <RouterApp />
        </BrowserRouter>
    </Provider>
)

export default App
