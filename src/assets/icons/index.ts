import castle from './castle.svg'
import dollar from './dollar.svg'
import egypt from './egypt.svg'
import energy from './energy.svg'
import gear from './gear.svg'
import levelButton from './level-button.svg'
import locomotive from './locomotive.svg'
import mammoth from './mammoth.svg'
import menuButton from './menu-button.svg'
import moneyButton from './money-button.svg'
import plus from './plus.svg'
import spaceship from './spaceship.svg'
import title from './title.svg'

export default {
    castle,
    dollar,
    egypt,
    energy,
    gear,
    levelButton,
    locomotive,
    mammoth,
    menuButton,
    moneyButton,
    plus,
    spaceship,
    title,
}
