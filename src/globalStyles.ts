import { createGlobalStyle } from 'styled-components'
import AmaticSCRegular from './vendor/fonts/AmaticSC-Regular.ttf'
import AmaticSCBold from './vendor/fonts/AmaticSC-Bold.ttf'

export const bgPrimaryColor = 'var(--bg-primary)'
export const bgSecondaryColor = 'var(--bg-secondary)'
export const bgSecondaryLightColor = 'var(--bg-secondary-light)'
export const greenLightColor = 'var(--green-light)'
export const greenBrightColor = 'var(--green-bright)'
export const greenDarkColor = 'var(--green-dark)'
export const brownBrightColor = 'var(--brown-bright)'
export const brownDarkColor = 'var(--brown-dark)'
export const beigeColor = 'var(--beige)'

export const titleHeight = 'var(--title-height)'

const GlobalStyle = createGlobalStyle`
    *, *::before, *::after {
      box-sizing: border-box;
      font-family: 'AmaticSC-Regular', Arial, Helvetica, sans-serif;
    }
    
    @font-face {
        font-family: 'AmaticSC-Regular';
        font-style: normal;
        font-weight: 400;
        src: url(${AmaticSCRegular}) format('ttf');
    }

    @font-face {
        font-family: 'AmaticSC-Bold';
        font-style: normal;
        font-weight: 700;
        src: url(${AmaticSCBold}) format('ttf');
    }

    :root {
        --bg-primary: radial-gradient(
            47.32% 66.25% at 48.45% 44.86%,
            #7c4737 0%,
            #744436 25%,
            #5e3d34 63%,
            #433431 100%
        );
        --bg-secondary: #f8a45b;
        --bg-secondary-light: #EAC991;

        --green-light: #e3ffbb;
        --green-bright: #95d543;
        --green-dark: #318063;

        --brown-bright: #96440c;
        --brown-dark: #5e3d34;

        --beige: #FFF6E6;

        --title-height: 150px;

        @media screen and (max-width: 767px) {
            --title-height: 100px;
        }
    }

    body {
        margin: 0;
        padding: 0;
        font-family: 'AmaticSC-Regular', Arial, Helvetica, sans-serif;
        font-weight: 400;
        font-style: normal;
        background: ${bgPrimaryColor} no-repeat;
        text-align: center;
        box-sizing: border-box;
    }

    h1 {
        font-family: 'AmaticSC-Bold', Arial, Helvetica, sans-serif;
        font-weight: 700;
        font-size: 48px;
        margin: 0;
        padding: 0;
    }

    h2 {
        font-family: 'AmaticSC-Bold', Arial, Helvetica, sans-serif;
        font-weight: 700;
        font-size: 48px;
        margin: 0;
        padding: 0;
        line-height: 1.7;

        @media screen and (max-width: 767px) {
            font-size: 36px;
            line-height: 1.5;
        }
    }
`

export default GlobalStyle
