import { lazy } from 'react'

export const Dashboard = lazy(() => import(/* webpackChunkName: "dashboard" */ './dashboard'))
export const Shop = lazy(() => import(/* webpackChunkName: "shop" */ './shop'))

