import styled from 'styled-components'
import {
    greenDarkColor,
    greenLightColor,
    greenBrightColor,
} from '../../globalStyles'

export const Section = styled.section`
    width: 100%;
    display: flex;
    justify-content: space-around;
    padding-bottom: 40px;
    max-width: 1440px;
    margin: auto;

    @media screen and (max-width: 1023px) {
        flex-direction: column;
    }
`

export const Content = styled.div`
    display: flex;
    justify-content: flex-end;
    align-items: flex-end;
    height: 600px;
    width: 100%;
    position: relative;
`

export const ProgressBar = styled.progress`
    transform: rotate(-90deg);
    transform-origin: left top;
    width: 520px;
    height: 60px;
    appearance: none;
    border-radius: 12px;
    border: 3px solid ${greenDarkColor};
    background: ${greenLightColor};
    position: absolute;
    bottom: -20px;
    left: 25px;

    &::-webkit-progress-value {
        background-color: ${greenBrightColor};
        border-radius: 9px 0 0 9px;
    }

    &::-webkit-progress-bar {
        background-color: ${greenLightColor};
        border-radius: 12px;
    }
`

export const Character = styled.img`
    width: 62%;
    margin-bottom: 40px;
    margin-right: 40px;

    @media screen and (max-width: 1023px) {
        width: 50%;
    }
`

export const StyledUl = styled.ul`
    padding: 0;
    list-style: none;
    display: flex;
    flex-wrap: wrap;
`

export const StyledLi = styled.li`
  
`
