import React, { FC } from 'react'
import { Button, Container, Title } from '../../components'
import { Character, Content, ProgressBar, Section, StyledLi, StyledUl } from './dashboard.styled'
import Images from '../../assets/images'
import { useGetDashboardQuery } from '../../__data__/api'

const Dashboard: FC = () => {
    //временно, потом будем тащить из стора
    const progressValue = '10'
    const { data, error, isLoading } = useGetDashboardQuery()

    return (
        <Section>
            <Container size="sm">
                <Title>{data?.output?.titles?.character}</Title>
                <Content>
                    <ProgressBar max="100" value={progressValue} />
                    <Character src={Images[data?.output?.images?.currentCharacter || 'character']} alt="Персонаж." />
                </Content>
            </Container>
            <Container size="md">
                <Title>{data?.output?.titles?.era}</Title>
                <StyledUl>
                    {
                        data?.output?.fields?.map((level) => {
                            return (
                                <StyledLi key={level.id}>
                                    <Button icon={level.icon} size={160} />
                                </StyledLi>
                            )
                        })}
                </StyledUl>
            </Container>
        </Section>
    )
}

export default Dashboard
