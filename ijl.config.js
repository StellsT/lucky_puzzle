const pkg = require('./package')

module.exports = {
    apiPath: 'stubs/api',
    webpackConfig: {
        output: {
            publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`
        }
    },
    navigations: {
        'lucky-puzzle.main': '/lucky-puzzle'
    },
    features: {
        'lucky-puzzle': {
            'auth': { value: 'auth' },
            'shop': { value: 'shop' },
            'dashboard': { value: 'dashboard' }
            // add your features here in the format [featureName]: { value: string }
        },
    },
    config: {
        key: 'value'
    }
}
