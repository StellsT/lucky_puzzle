const router = require('express').Router()
const fs = require('fs')
const path = require('path')

const DELAY_TIME = 100

module.exports = router
    .get('/dashboard', (req, res) => {
        const filePath = path.resolve(__dirname, './dashboard/dashboard.json')

        fs.readFile(filePath, (err, data) => {
            if (err) {
                return res.status(404).send({ error: `No readable file at ${filePath}` })
            }
            setTimeout(() => res.type('json').send(data), DELAY_TIME)
        })
    })
