module.exports = {
    parser: '@typescript-eslint/parser',
    env: {
        node: true,
        browser: true,
        commonjs: true,
        es2021: true,
    },
    extends: ['plugin:react/recommended', 'plugin:@typescript-eslint/recommended'],
    parserOptions: {
        ecmaVersion: 12,
        sourceType: 'module'
    },
    settings: {
        'react': {
            'version': 'detect'
        }
    },
    rules: {
        '@typescript-eslint/no-var-requires': 'off',
        'no-console': 'warn',
        'no-magic-numbers': 'warn',
        indent: ['warn', 4],
        'react/display-name': 'off',
        semi: ['warn', 'never'],
	    'object-curly-spacing': ['warn', 'always'],
	    '@typescript-eslint/no-use-before-define': ['error'],
	    'max-len': ["error", { "code": 120 }]
    },
    globals: {
    },
}
