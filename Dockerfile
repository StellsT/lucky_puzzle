# Use starter image
FROM node:16

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Install app dependencies
COPY package.json /usr/src/app/

# Bundle app source
COPY . /usr/src/app

# Expose port
EXPOSE 80

# Default command to run
CMD ["sh", "docker-start.sh"]